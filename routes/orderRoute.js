// import express, controller, auth
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/OrderController");

// Create order
router.post("/", auth.verifyToken, (req, res) => {
  const userId = req.user.id; // Get the user ID from the authenticated user

  orderController.createOrder(userId, req.body)
    .then(result => {
      if (result.error) {
        res.status(500).json({ error: result.error });
      } else {
        res.status(200).json(result);
      }
    })
    .catch(error => {
      res.status(500).json({ error: error.message });
    });
});

// Get user orders - user level
router.get("/", auth.verifyToken, async (req, res) => {
  try {
    const userId = req.user.id; // Get the user ID from the authenticated user
    const orders = await orderController.getUserOrders(userId);
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Get all orders - admin only
router.get("/all", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const orders = await orderController.getAllOrders();
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Get all pending orders - admin only
router.get("/pending", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const pendingOrders = await orderController.getPendingOrders();
    res.status(200).json(pendingOrders);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Get monthly income - admin only
router.post("/income", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const startMonth = parseInt(req.body.startMonth);
    const startYear = parseInt(req.body.startYear);
    const endMonth = parseInt(req.body.endMonth);
    const endYear = parseInt(req.body.endYear);

    const income = await orderController.getMonthlyIncome(startMonth, startYear, endMonth, endYear);
    res.status(200).json({ income });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Find user order by user ID - admin only
router.get("/find/user/:userId", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const userId = req.params.userId;
    const orders = await orderController.getUserOrders(userId);
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Find order by order ID - admin only
router.get("/find/order/:orderId", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const order = await orderController.getOrderById(orderId);
    res.status(200).json(order);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Update order - admin only
router.patch("/:id", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const updatedOrder = await orderController.updateOrder(req.params.id, req.body);
    res.status(200).json(updatedOrder);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Delete order - admin only
router.delete("/:id", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    await orderController.deleteOrder(req.params.id);
    res.status(200).json("Order has been deleted...");
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
