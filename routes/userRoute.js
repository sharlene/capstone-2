// Import express, controller, auth
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/UserController");

// Routes

// Check existing user
router.post("/check-email", async (req, res) => {
  try {
    const result = await userController.checkEmailExists(req.body);
    console.log(result); // Log the result
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Register new user
router.post("/register", async (req, res) => {
  try {
    const result = await userController.registerUser(req.body);
    console.log(result); // Log the result
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Login user
router.post("/login", async (req, res) => {
  try {
    const result = await userController.loginUser(req.body);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Route for regular users to retrieve their own data
router.get('/me', auth.verifyToken, async (req, res) => {
  try {
    const userId = req.user.id; 

    const result = await userController.getUser(userId, userId, false); // Pass authenticated user ID and isAdmin=false

    if (result.error) {
      return res.status(404).json({ error: result.error });
    }

    return res.status(200).json(result.user);
  } catch (error) {
    console.error(error); // Log the error for debugging purposes
    return res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all user data - admin only
router.get('/admin/all', auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const authenticatedUserId = req.user.id; 

    const result = await userController.getAllUsers(authenticatedUserId);

    if (result.error) {
      return res.status(404).json({ error: result.error });
    }

    return res.status(200).json(result.users);
  } catch (error) {
    console.error(error); // Log the error for debugging purposes
    return res.status(500).json({ error: 'Internal server error' });
  }
});

// Route for admins to retrieve any user's data
router.get('/admin/:userId', auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const userId = req.params.userId;
    const authenticatedUserId = req.user.id; 
    const isAdmin = req.user.isAdmin; 

    const result = await userController.getUser(userId, authenticatedUserId, isAdmin);

    if (result.error) {
      return res.status(404).json({ error: result.error });
    }

    return res.status(200).json(result.user);
  } catch (error) {
    console.error(error); // Log the error for debugging purposes
    return res.status(500).json({ error: 'Internal server error' });
  }
});

// Update user details - admin only
router.patch("/admin/:userId", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const result = await userController.updateUser(req.params.userId, req.body, req.user.id, true); // Pass authenticatedUserId and isAdmin as true
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

// Delete user - admin only
router.delete("/admin/:userId", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const result = await userController.deleteUser(req.params.userId, req.user.id, true); // Pass authenticatedUserId and isAdmin as true
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

// Delete user - admin only
router.delete("/admin/:userId", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const result = await userController.deleteUser(req.params.userId, req.user.id, true); // Pass authenticatedUserId and isAdmin as true
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;

// Update user details - user level
router.patch("/:userId", auth.verifyToken, async (req, res, next) => {
  try {
    if (req.user.id === req.params.userId) {
      const result = await userController.updateUser(req.params.userId, req.body, req.user.id, false); // Pass authenticatedUserId and isAdmin as false
      res.status(200).send(result);
    } else {
      res.status(403).json("You are not allowed to do that!");
    }
  } catch (err) {
    res.status(500).send(err);
  }
});


