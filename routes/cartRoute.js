const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/CartController");
const orderController = require("../controllers/OrderController");

// Create or Update - user level
router.post("/", auth.verifyToken, async (req, res) => {
  try {
    const userId = req.user.id; // Get the user ID from the authenticated user
    const requestData = {
      userId: userId,
      products: req.body
    }
    const savedCart = await cartController.createCart(requestData);
    res.status(200).json(savedCart);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});


// Dekete - user level
router.delete("/", auth.verifyToken, async (req, res) => {
  try {
    const userId = req.user.id;
    await cartController.deleteCart(userId);
    res.status(200).json("Cart has been deleted...");
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Get cart - user level
router.get("/", auth.verifyToken, async (req, res) => {
  try {
    const authenticatedUserId = req.user.id;
    const { cart, error } = await cartController.getUserCart(authenticatedUserId);

    if (error) {
      return res.status(404).json({ error });
    }

    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Checkout - user level
router.post("/checkout", auth.verifyToken, async (req, res) => {
  try {
    const userId = req.user.id;
    const cart = await cartController.getUserCart(userId);
    const order = await orderController.createOrder({
      userId,
      products: cart.products,
      address: req.body.address,
    });
    // Clear the cart after successful checkout
    await cartController.deleteCart(userId);
    res.status(200).json(order);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Get all cart - admin level
router.get("/all", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const carts = await cartController.getAllCarts();
    res.status(200).json(carts);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
