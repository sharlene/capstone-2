// Import express, controller, auth, multer
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/ProductController");
const multer = require("multer");
const upload = multer({ dest: "uploads/" })





// Routes

// Register new product with image upload
router.post("/register", auth.verifyTokenAndAdmin, upload.single("image"), async (req, res) => {
  try {
    const isAdmin = req.user.isAdmin;
    const result = await productController.registerProduct(req, isAdmin); // Pass the entire request object to the controller
    console.log(result);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});


// Retrieve all products
router.get("/", async (req, res) => {
  try {
    const result = await productController.getAllProducts();

    if (result.error) {
      res.status(500).json({ error: result.error });
    } else {
      res.status(200).json(result.products);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Retrieve in-stock products
router.get("/instock", async (req, res) => {
  try {
    const result = await productController.getInStockProducts();

    if (result.error) {
      res.status(500).json({ error: result.error });
    } else {
      res.status(200).json(result.products);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Archive a product -admin only
router.patch("/archive/:slug", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const { slug } = req.params;
    const isAdmin = req.user.isAdmin;

    const result = await productController.archiveProduct(slug, isAdmin);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Activate a product - admin only
router.patch("/activate/:slug", auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const { slug } = req.params;
    const isAdmin = req.user.isAdmin;

    const result = await productController.activateProduct(slug, isAdmin);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});



// Update product details - admin only
router.patch("/:slug", auth.verifyTokenAndAdmin, upload.single("image"), async (req, res) => {
  try {
    const isAdmin = req.user.isAdmin;
    const slug = req.params.slug; // Get the slug from the request params
    const updatedData = req.body;
    const imageFile = req.file; // Get the uploaded image file

    const result = await productController.updateProduct(slug, updatedData, isAdmin, imageFile);
    console.log(result);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});



// Delete a product item (Admin only)
router.delete("/:slug", auth.verifyTokenAndAdmin, (req, res) => {
  const isAdmin = req.user.isAdmin;
  const slug = req.params.slug;

  productController.deleteProduct(slug, isAdmin)
    .then(result => {
      console.log(result);
      res.send(result);
    });
});



// Retrieve a single product by ID
router.get("/:slug", async (req, res) => {
  try {
    const slug = req.params.slug;
    const result = await productController.getProductBySlug(slug);

    if (result.error) {
      res.status(404).json({ error: result.error });
    } else {
      res.status(200).json(result.product);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Retrieve products by category
router.get("/:category", async (req, res) => {
  try {
    const category = req.params.category;
    const result = await productController.getProductsByCategory(category);

    if (result.error) {
      res.status(500).json({ error: result.error });
    } else {
      res.status(200).json(result.products);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});


module.exports = router;
