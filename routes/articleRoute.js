// Import dependencies
const express = require('express');
const router = express.Router();
const auth = require('../auth');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const isValid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error('Invalid image type');

    if (isValid) {
      uploadError = null;
    }
    cb(uploadError, 'public/uploads');
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(' ').join('-');
    const extension = FILE_TYPE_MAP[file.mimetype];
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  },
});
const uploadOptions = multer({ storage: storage });

// Import models
const Product = require('../models/Product');
const User = require('../models/User');
const Article = require('../models/Article');

// Check if user is authorized to update details
const isAuthorizedToUpdateProduct = async (userId, authenticatedUserId, isAdmin) => {
  if (isAdmin) {
    return true;
  }

  if (userId === authenticatedUserId) {
    const user = await User.findById(userId);
    return user !== null;
  }

  return false;
};

// Add new article
router.post('/add', auth.verifyTokenAndAdmin, uploadOptions.single('image'), async (req, res) => {
  try {
    const isAdmin = req.user.isAdmin;
    const existingArticle = await Article.findOne({ name: req.body.title });

    if (existingArticle) {
      return res.status(400).json({ error: 'Article is already added.' });
    }

    const file = req.file;
    if (!file) {
      return res.status(400).json({ error: 'No image in the request.' });
    }

    const fileName = file.filename;
    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;

    let newArticle = new Article({
      title: req.body.name,
      desc: req.body.desc,
      content: req.body.content,
      image: `${basePath}${fileName}`,
      slug: req.body.slug,
    });

    const addedArticle = await newArticle.save();
    res.status(201).json({ message: 'Article successfully added!', article: addedArticle });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// Retrieve all articles
router.get('/', async (req, res) => {
  try {
    const articleList = await Article.find();
    res.status(200).json(articleList);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// Retrieve specific article
router.get('/:id', async (req, res) => {
  try {
    const article = await Product.findById(req.params.id);
    if (!article) {
      return res.status(404).json({ error: 'Article not found.' });
    }
    res.status(200).json(article);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// Update article details
router.patch('/:id', auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const isAdmin = req.user.isAdmin;
    const article = await Article.findById(req.params.id);

    if (!article) {
      return res.status(404).json({ error: 'Article not found.' });
    }

    if (!isAuthorizedToUpdateArticle(article.userId, req.user.userId, isAdmin)) {
      return res.status(403).json({ error: 'Unauthorized to update article details.' });
    }

    const updatedArticle = await Article.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.status(200).json(updatedArticle);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// Delete an article
router.delete('/:id', auth.verifyTokenAndAdmin, async (req, res) => {
  try {
    const isAdmin = req.user.isAdmin;
    const article = await Article.findById(req.params.id);

    if (!article) {
      return res.status(404).json({ error: 'Articlenot found.' });
    }

    if (!isAuthorizedToDeleteArticle(article.userId, req.user.userId, isAdmin)) {
      return res.status(403).json({ error: 'Unauthorized to delete the article.' });
    }

    await Article.findByIdAndRemove(req.params.id);
    res.status(200).json({ message: 'Article deleted successfully.' });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
