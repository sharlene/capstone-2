const Cart = require("../models/Cart");
const Product = require("../models/Product");


// CREATE or UPDATE CART
module.exports.createCart = async (requestData) => {
  try {
    const { userId, products } = requestData;
    const existingCart = await Cart.findOne({ userId });

    // If a cart already exists, add the new products to it
    if (existingCart) {
      products.forEach((product) => {
        const productExists = existingCart.products.find((p) => p.productId === product.productId);
        if (productExists) {
          productExists.quantity += product.quantity;
          productExists.totalPrice = productExists.quantity * productExists.price;
        } else {
          existingCart.products.push({
            productId: product.productId,
            quantity: product.quantity,
            price: product.price,
            totalPrice: product.price * product.quantity,
          });
        }
      });
      existingCart.totalAmount = existingCart.products.reduce((total, product) => {
        return total + product.totalPrice;
      }, 0);
      const updatedCart = await existingCart.save();
      return updatedCart;
    }

    // If no cart exists, create a new one
    const newCart = new Cart({
      userId: userId,
      products: products.map((product) => ({
        productId: product.productId,
        quantity: product.quantity,
        price: product.price,
        totalPrice: product.price * product.quantity,
      })),
    });

    const savedCart = await newCart.save();
    return savedCart;
  } catch (error) {
    throw error;
  }
};


module.exports.updateCart = async (cartId, updatedData, authenticatedUserId, isAdmin) => {
  try {
    // Check if the user is an admin
    if (isAdmin) {
      const updatedCart = await Cart.findByIdAndUpdate(
        cartId,
        { $set: updatedData },
        { new: true }
      );
      return updatedCart;
    }

    // Check if the requested user is the authenticated user
    const cart = await Cart.findById(cartId);

    if (!cart) {
      throw new Error("No cart found. Please create a cart.");
    }

    if (cart.userId === authenticatedUserId) {
      const updatedCart = await Cart.findByIdAndUpdate(
        cartId,
        { $set: updatedData },
        { new: true }
      );
      return updatedCart;
    }

    // User is not authorized to update another user's cart
    throw new Error("Not authorized to update this cart.");
  } catch (error) {
    throw error;
  }
};


// DELETE
module.exports.deleteCart = async (cartId, authenticatedUserId, isAdmin) => {
  try {
    // Check if the user is an admin
    if (isAdmin) {
      await Cart.findByIdAndDelete(cartId);
      return "Cart has been deleted...";
    }

    // Check if the requested user is the authenticated user
    const cart = await Cart.findById(cartId);

    if (!cart) {
      throw new Error("No cart found.");
    }

    if (cart.userId === authenticatedUserId) {
      await Cart.findByIdAndDelete(cartId);
      return "Cart has been deleted...";
    }

    // User is not authorized to delete another user's cart
    throw new Error("Not authorized to delete this cart.");
  } catch (error) {
    throw error;
  }
};

// Get user cart
module.exports.getUserCart = async (authenticatedUserId) => {
  try {
    // Find the cart by user ID
    const cart = await Cart.findOne({ userId: authenticatedUserId });

    if (!cart) {
      return { error: "Cart not found." };
    }

    // Fetch the prices of the products from the database
    const productIds = cart.products.map((product) => product.productId);
    const fetchedProducts = await Product.find({ _id: { $in: productIds } });

    // Create an object to store the prices mapped by product ID
    const productPrices = {};
    fetchedProducts.forEach((product) => {
      productPrices[product._id] = product.price;
    });

    // Update the products in the cart with price and total price
    cart.products.forEach((product) => {
      const price = productPrices[product.productId];
      product.price = price;
      product.totalPrice = price * product.quantity;
    });

    // Calculate the total amount
    cart.totalAmount = cart.products.reduce((total, product) => {
      return total + product.totalPrice;
    }, 0);

    await cart.save();

    return { cart };
  } catch (error) {
    throw error;
  }
};




// GET ALL CARTS - admin level
module.exports.getAllCarts = async () => {
  try {
    const carts = await Cart.find();
    return carts;
  } catch (error) {
    throw error;
  }
};

// CHECKOUT CART
module.exports.checkoutCart = async (cartId, authenticatedUserId) => {
  try {
    const cart = await Cart.findById(cartId);

    if (!cart) {
      throw new Error("No cart found.");
    }

    if (cart.userId !== authenticatedUserId) {
      throw new Error("Not authorized to checkout this cart.");
    }

    // Fetch the prices of the products from the database
    const productIds = cart.products.map((product) => product.productId);
    const fetchedProducts = await Product.find({ _id: { $in: productIds } });

    // Create an object to store the prices mapped by product ID
    const productPrices = {};
    fetchedProducts.forEach((product) => {
      productPrices[product._id] = product.price;
    });

    // Update the products in the cart with price and total price
    cart.products.forEach((product) => {
      const price = productPrices[product.productId];
      product.price = price;
      product.totalPrice = price * product.quantity;
    });

    // Calculate the total amount of the cart
    const amount = cart.products.reduce((total, product) => {
      return total + product.totalPrice;
    }, 0);

    // Create an order based on the cart
    const newOrder = {
      userId: cart.userId,
      products: cart.products,
      amount,
      address: cart.address,
      status: "pending",
    };

    // Delete the cart
    await Cart.findByIdAndDelete(cartId);

    return newOrder;
  } catch (error) {
    throw error;
  }
};

