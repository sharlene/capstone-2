// import models
const User = require("../models/User");
const auth = require("../auth");
const bcrypt = require("bcrypt");


// export modules

// Check email exists module
module.exports.checkEmailExists = (requestBody) => {
  return User.find({ email: requestBody.email }).then((result) => {
    if (result.length > 0) {
      return true
    }
    return false
  });
};

// Register email module
module.exports.registerUser = async (requestBody) => {
    // Check if email or mobileNo is already registered
    const existingUser = await User.findOne({
      $or: [{ email: requestBody.email }, { mobileNo: requestBody.mobileNo }],
    });

    if (existingUser) {
      if (existingUser.email === requestBody.email) {
        return { error: "Email is already registered." };
      } else {
        return { error: "Mobile number is already registered." };
      }
    }

    // Create a new user
    let newUser = new User({
      firstName: requestBody.firstName,
      lastName: requestBody.lastName,
      email: requestBody.email,
      mobileNo: requestBody.mobileNo,
      password: bcrypt.hashSync(requestBody.password, 10),
    });

    return newUser.save().then((registeredUser, error) => {
      if(error){
        return error
      } return { message: "User successfully registered!" }
    })
  }

// Login module
module.exports.loginUser = (requestBody) => {
  return User.findOne({ email: requestBody.email }).then(result => {
    if (result === null) {
      return { error: "The user doesn't exist." }
    }

    const correctPassword = bcrypt.compareSync(requestBody.password, result.password)

    if (correctPassword) {
        console.log('User successfully logged in!')
        const { password, ...userData } = result.toObject(); // Destructure and omit the password property
        return {
        access: auth.createAccessToken(result.toObject()),
        userData: userData
        
      }
    }
    return { error: 'The email and password combination is not correct' } 
  })
}


// Check if user is authorized to update details
module.exports.isAuthorizedToUpdateUser = async (
  userId,
  authenticatedUserId,
  isAdmin
) => {
  if (isAdmin) {
    return true; // Admin user is authorized to update any user
  }

  if (userId === authenticatedUserId) {
    const user = await User.findById(userId);
    return user !== null;
  }

  return false;
};

// Update password/details
module.exports.updateUser = async (
  userId,
  newReqBody,
  authenticatedUserId,
  isAdmin
) => {
  try {
    const isAuthorized = await module.exports.isAuthorizedToUpdateUser(
      userId,
      authenticatedUserId,
      isAdmin
    );

    if (!isAuthorized && !isAdmin) {
      return { error: "You are not authorized to update this user." };
    }

    let updatedUser = await User.findByIdAndUpdate(
      userId,
      {
        firstName: newReqBody.firstName,
        lastName: newReqBody.lastName,
        email: newReqBody.email,
        mobileNo: newReqBody.mobileNo,
        password: newReqBody.password
          ? bcrypt.hashSync(newReqBody.password, 10)
          : undefined,
        isAdmin: isAdmin ? newReqBody.isAdmin : undefined,
      },
      { new: true }
    );

    if (!updatedUser) {
      return { error: "User not found." };
    }

    return {
      message: "User details updated successfully!",
      user: updatedUser,
    };
  } catch (error) {
    return { error: error.message };
  }
};

// Delete user
module.exports.deleteUser = async (
  userId,
  authenticatedUserId,
  isAdmin
) => {
  try {
    const isAuthorized = await module.exports.isAuthorizedToUpdateUser(
      userId,
      authenticatedUserId,
      isAdmin
    );

    if (!isAuthorized) {
      return { error: "You are not authorized to delete this user." };
    }

    const deletedUser = await User.findByIdAndDelete(userId);

    if (!deletedUser) {
      return { error: "User not found." };
    }

    return {
      message: "User deleted successfully!",
      user: deletedUser,
    };
  } catch (error) {
    return { error: error.message };
  }
};

// Get single user data
module.exports.getUser = async (
  userId,
  authenticatedUserId,
  isAdmin
) => {
  try {
    const isAuthorized = await module.exports.isAuthorizedToUpdateUser(
      userId,
      authenticatedUserId,
      isAdmin
    );

    if (!isAuthorized) {
      return { error: "You are not authorized to access this user's data." };
    }

    const user = await User.findById(userId).select("-password");

    if (!user) {
      return { error: "User not found." };
    }

    return {
      message: "User data retrieved successfully!",
      user: user,
    };
  } catch (error) {
    return { error: error.message };
  }
};

// Get all user data - admin level
module.exports.getAllUsers = async (authenticatedUserId) => {
  try {
    const isAdmin = true; // Set isAdmin to true since this function is specifically for admins

    // Check if the authenticated user is an admin
    const isAuthorized = await module.exports.isAuthorizedToUpdateUser(authenticatedUserId, authenticatedUserId, isAdmin);
    if (!isAuthorized) {
      return { error: "You are not authorized to access all user data." };
    }

    // Fetch all users from the database (excluding the password field)
    const users = await User.find().select("-password");

    return {
      message: "All user data retrieved successfully!",
      users: users,
    };
  } catch (error) {
    return { error: error.message };
  }
};
