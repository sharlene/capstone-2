// import models
const Product = require("../models/Product");
const User = require("../models/User");
const multer = require("multer");
const slugify = require("slugify");

// export modules

// Create Multer storage configuration
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/"); // Destination folder to store uploaded images
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9); // Generate a unique filename
    cb(null, file.fieldname + "-" + uniqueSuffix); // Set the filename
  },
})

// Create Multer upload instance
const upload = multer({ storage: storage });


// Check if user is authorized to update details
module.exports.isAuthorizedToUpdateProduct = async (
  userId,
  authenticatedUserId,
  isAdmin
) => {
  if (isAdmin) {
    return true; // Admin user is authorized to update any user
  }

  if (userId === authenticatedUserId) {
    const user = await User.findById(userId);
    return user !== null;
  }

  return false;
};


// Register product module with image upload
module.exports.registerProduct = async (req, isAdmin) => {
  try {
    if (!isAdmin) {
      return { error: "Only the admin can register a new product." };
    }

    // Check if product already registered
    const existingProduct = await Product.findOne({ name: req.body.name });

    if (existingProduct) {
      return { error: "Product is already registered." };
    }

    // Generate the slug
    const slug = slugify(req.body.name, { lower: true, replacement: "-" });

    // Create a new product
    let newProduct = new Product({
      name: req.body.name,
      desc: req.body.desc,
      categories: req.body.categories,
      size: req.body.size,
      color: req.body.color,
      price: req.body.price,
      image: req.file ? req.file.filename : undefined,
      slug: slug, // Store the generated slug
    });

    const registeredProduct = await newProduct.save();
    return { message: "Product successfully registered!", product: registeredProduct };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};


// Update product details - admin only
module.exports.updateProduct = async (slug, updatedData, isAdmin, imageFile) => {
  try {
    if (!isAdmin) {
      return { error: "Only the admin can update product details." };
    }

    const product = await Product.findOne({ slug: slug });
    if (!product) {
      return { error: "Product not found." };
    }

    // Update product details
    product.name = updatedData.name;
    product.desc = updatedData.desc;
    product.categories = updatedData.categories;
    product.size = updatedData.size;
    product.color = updatedData.color;
    product.price = updatedData.price;
    if (imageFile) {
      product.image = imageFile.filename; // Update the image filename if a new image is provided
    }

    const updatedProduct = await product.save();
    return { message: "Product details updated.", product: updatedProduct };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};



  
// Delete a product item -admin only
module.exports.deleteProduct = async (slug, isAdmin) => {
  try {
    if (!isAdmin) {
      return { error: "Only the admin can delete a product." };
    }

    const product = await Product.findOne({ slug: slug });
    if (!product) {
      return { error: "Product not found." };
    }

    await Product.deleteOne({ slug: slug });
    return { message: "Product deleted successfully." };
  } catch (error) {
    console.log(error);
    return { error: "Failed to delete product." };
  }
};



// Archive a product -admin only
module.exports.archiveProduct = async (slug, isAdmin) => {
  try {
    if (!isAdmin) {
      return { error: "Only the admin can archive a product." };
    }

    const product = await Product.findOne({ slug: slug });
    if (!product) {
      return { error: "Product not found." };
    }

    product.inStock = false;
    const updatedProduct = await product.save();
    return { message: "Product archived.", product: updatedProduct };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};

  
// Activate a product -admin only
module.exports.activateProduct = async (slug, isAdmin) => {
  try {
    if (!isAdmin) {
      return { error: "Only the admin can activate a product." };
    }

    const product = await Product.findOne({ slug: slug });
    if (!product) {
      return { error: "Product not found." };
    }

    product.inStock = true;
    const updatedProduct = await product.save();
    return { message: "Product activated.", product: updatedProduct };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};


// Retrieve all products
module.exports.getAllProducts = () => {
    return Product.find()
      .then((products) => {
        return { products };
      })
      .catch((error) => {
        console.log(error);
        return { error: error.message };
      });
  };

// Retrieve in-stock products
module.exports.getInStockProducts = () => {
    return Product.find({ inStock: true })
      .then((products) => {
        return { products };
      })
      .catch((error) => {
        console.log(error);
        return { error: error.message };
      });
  };

// Retrieve single product
module.exports.getProductBySlug = (slug) => {
  return Product.findOne({ slug: slug })
    .then((product) => {
      if (!product) {
        return { error: "Product not found." };
      }
      return { product };
    })
    .catch((error) => {
      console.log(error);
      return { error: error.message };
    });
};

// Retrieve products by category
module.exports.getProductsByCategory = async (category) => {
  try {
    const products = await Product.find({ categories: category });
    return { products };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};
