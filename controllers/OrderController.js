// import models
const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.createOrder = async (userId, request_body) => {
  const { products, address, paymentMethod } = request_body;

  try {
    // Fetch the prices of the products from the database
    const productIds = products.map((product) => product.productId);
    const fetchedProducts = await Product.find({ _id: { $in: productIds } });

    // Create an object to store the prices mapped by product ID
    const productPrices = {};
    fetchedProducts.forEach((product) => {
      productPrices[product._id] = product.price;
    });

    // Calculate the order amount based on the fetched prices
    const amount = products.reduce((total, product) => {
      const price = productPrices[product.productId];
      return total + price * product.quantity;
    }, 0);

    // Create the new order
    const newOrder = new Order({
      userId,
      products,
      amount,
      address,
      status: 'pending',
      paymentMethod,
      isPaid: false,
    });

    // Save the order to the database
    const savedOrder = await newOrder.save();

  } catch (error) {
    return {
      error: error.message,
    };
  }
};

// Update the order - Admin Only
module.exports.updateOrder = async (orderId, updateFields) => {
  try {
    const updatedOrder = await Order.findByIdAndUpdate(
      orderId,
      { $set: updateFields },
      { new: true }
    );
    return updatedOrder;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Delete order - Admin Only
module.exports.deleteOrder = async (orderId) => {
  try {
    await Order.findByIdAndDelete(orderId);
    return "Order has been deleted...";
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get user order by ID
module.exports.getUserOrders = async (userId) => {
  try {
    const orders = await Order.find({ userId });
    return orders;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get user order by order ID
module.exports.getOrderById = async (orderId) => {
  try {
    const order = await Order.findById(orderId);
    return order;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get all orders - admin only
module.exports.getAllOrders = async () => {
  try {
    const orders = await Order.find();
    return orders;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get all pending orders - admin only
module.exports.getPendingOrders = async () => {
  try {
    const pendingOrders = await Order.find({ status: "Pending" });
    return pendingOrders;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get monthly income - admin only
module.exports.getMonthlyIncome = async (startMonth, startYear, endMonth, endYear) => {
  const startDate = new Date(startYear, startMonth - 1, 1);
  const endDate = new Date(endYear, endMonth - 1, 0);

  try {
    const income = await Order.aggregate([
      { $match: { createdAt: { $gte: startDate, $lte: endDate } } },
      {
        $group: {
          _id: null,
          total: { $sum: "$amount" },
        },
      },
    ]);

    return {
      message: `Total income for ${startMonth}/${startYear} to ${endMonth}/${endYear}:`,
      income: income[0]?.total || 0
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
