// Server dependencies
const jwt = require("jsonwebtoken");

// export modules
module.exports.createAccessToken = (user) => {
  const userData = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
    firstName: user.firstName,
    lastName: user.lastName,
    mobileNo: user.mobileNo,
  };

  return jwt.sign(userData, process.env.JWT_SEC, {});
};
;

module.exports.verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    let token = authHeader.split(" ")[1];
    jwt.verify(token, process.env.JWT_SEC, (err, user) => {
      if (err) {
        res.status(403).json("Token is not valid!");
      } else {
        req.user = user;
        next();
      }
    });
  } else {
    return res.status(401).json("You are not authenticated!");
  }
};

module.exports.verifyTokenAndAdmin = (req, res, next) => {
  module.exports.verifyToken(req, res, () => {
    if (req.user.isAdmin) {
      next();
    } else {
      res.status(403).json("You are not allowed to do that!");
    }
  });
};

module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);

    return jwt.verify(token, process.env.JWT_SEC, (error, data) => {
      if (error) {
        return null;
      }

      return jwt.decode(token, { complete: true }).payload;
    });
  } else {
    return null;
  }
};
