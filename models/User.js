// import mongoose
const mongoose = require("mongoose")


// create schema model
const UserSchema = new mongoose.Schema(
    {
        firstName:{type: String, required:[true, `First Name is required.`]},
        lastName:{type: String, required:[true, `Last Name is required.`]},
        email:{type: String, required:[true, `Unique email is required.`], unique:true},
        mobileNo: {type: String, required:[true, `Unique mobile number is required.`], unique:true},
        password:{type: String, required:[true, `Password is required.`]},
        
        isAdmin : {
            type: Boolean,
            default: false, 
            required:true
        }
    },
    {timestamps:true}
);

// module export
module.exports = mongoose.model("Users", UserSchema);