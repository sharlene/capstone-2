// import mongoose
const mongoose = require("mongoose")
const slugify = require("slugify");

// create schema model
const ProductSchema = new mongoose.Schema(
    {
        name: { type: String, required: true, unique: true },
        desc: { type: String, required: true, },
        categories: { type: Array },
        size: { type: Array },
        color: { type: Array },
        inStock: {type: Boolean, default: false},
        price: { type: Number, required: true },
        image: { type: String },
        slug: {type: String, unique:true}
      },
    {timestamps:true}
);

// Generate slug before saving the product
ProductSchema.pre("save", function (next) {
  this.slug = slugify(this.name, { lower: true, replacement: "-", remove: /[*+~.()'"!:@]/g });
  next();
});

// module export
module.exports = mongoose.model("Products", ProductSchema);