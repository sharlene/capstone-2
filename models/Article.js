// import mongoose
const mongoose = require("mongoose")
const slugify = require("slugify");


// create schema model
const ArticleSchema = new mongoose.Schema(
    {
        title: { type: String, required: true, unique: true },
        desc: { type: String, required: true, },
        content: { type: String },
        image: {type: String},
        slug: { type: String, unique: true },
        
      },
    {timestamps:true}
);

// Generate and store slug before saving the blog article
ArticleSchema.pre("save", function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

// module export
module.exports = mongoose.model("Article", ArticleSchema);