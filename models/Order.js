const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
  {
    userId: { type: String, required: true },
    products: [
      {
        name: {
          type: String
        },

        size: {
          type: String
         },
         
        color: {
          type: String
         },

        quantity: {
          type: Number,
          default: 1,
        },
        price: {
          type: Number,
        },
        totalPrice: {
          type: Number
        },
      },
    ],
    amount: { type: Number, required: true },
    address: { type: Object, required: true },
    status: { type: String, required: true },
    paymentMethod: {
      type: String,
      enum: ["Cash on Delivery", "GCash", "Maya", "Bank Transfer"],
      required: true,
    },
    isPaid: { type: Boolean, default: false },
  },
  { timestamps: true }
);

// Pre-save middleware to calculate the total price per product
OrderSchema.pre("save", function (next) {
  this.products.forEach((product) => {
    product.totalPrice = product.price * product.quantity;
  });

  this.amount = this.products.reduce((total, product) => {
    return total + product.totalPrice;
  }, 0);

  next();
});

module.exports = mongoose.model("Order", OrderSchema);
