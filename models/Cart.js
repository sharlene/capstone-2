const mongoose = require("mongoose");

const CartSchema = new mongoose.Schema(
  {
    userId: { type: String, required: true },
    products: [
      {
        productId: {
          type: String,
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
          default: 1,
        },
        price: {
          type: Number,
        },
        totalPrice: {
          type: Number,
        },
      },
    ],
    totalAmount: { type: Number, default: 0 },
  },
  { timestamps: true }
);

// Post-save middleware to calculate the total price per product and the total amount
CartSchema.post("save", function (doc) {
  doc.products.forEach((product) => {
    product.totalPrice = product.price * product.quantity;
  });

  doc.totalAmount = doc.products.reduce((total, product) => {
    return total + product.totalPrice;
  }, 0);

  doc.save();
});

module.exports = mongoose.model("Cart", CartSchema);
