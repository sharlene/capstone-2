Users
		// Check existing user
// Register new user
// User authentication - via auth.js
		// Login user
		// Route for regular users to retrieve their own data
		// Get all user data - admin level
		// Route for admins to retrieve any user's data
	// Update user details - admin level
		// Delete user - admin level
	// Update user details - user level

Products
// Register new product - admin only
// Retrieve all products
		// Retrieve in-stock products
// Archive a product - admin only
// Activate a product -admin only
// Update product details -admin only
		// Delete a product item -admin only
// Retrieve a single product by ID

Order
// Create order
	// Get user orders - user level
	// Get all orders - admin only
		// Get all pending orders - admin only
		// Get monthly income - admin only
		// Find user order by user ID - admin only
		// Find order by order ID - admin only
		// Update order - admin only
		// Delete order - admin only

Cart
	// Create cart
	// Update cart - user
		// Delete cart - user
		// Get user cart - user
		// Checkout cart - user
		// Get all carts - admin only