// Server dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Route dependencies
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

// Server setup
const app = express();

// Initialize environment variables
require("dotenv").config();

// Database connection & listener
mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Connected to MongoDB successfully!"));

// Middleware registration
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/api/users", userRoute);
app.use("/api/products", productRoute);
app.use("/api/orders", orderRoute);
app.use("/api/carts", cartRoute);

// Server listening
app.listen(process.env.PORT || 3000, () =>
  console.log(`The server is running on ${process.env.PORT || 3000}`)
);
